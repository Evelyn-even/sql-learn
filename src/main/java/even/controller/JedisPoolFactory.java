package even.controller;

import org.springframework.stereotype.Controller;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.net.URI;

@Controller
public class JedisPoolFactory {
    private static JedisPool jedisPool = null;

    private JedisPoolFactory(){

    }
    public static JedisPool getInstance() {
        if (jedisPool == null) {
            synchronized (JedisPoolFactory.class) {
                if (jedisPool == null) {
                    JedisPoolConfig config = new JedisPoolConfig();

                    config.setMaxTotal(64);
                    config.setMaxIdle(64);
                    config.setMinIdle(64);
                    config.setTestOnBorrow(true);
                    config.setTestOnReturn(true);
                    config.setTestWhileIdle(true);
                    config.setMaxWaitMillis(3000);
                    config.setMinEvictableIdleTimeMillis(60);
                    config.setTimeBetweenEvictionRunsMillis(30);
                    config.setBlockWhenExhausted(false);
                    URI uri = URI.create("redis://192.168.79.130:6379");

                    jedisPool = new JedisPool(config, uri, 2000, 2000);
                }
            }
        }
        return jedisPool;
    }
    public static String getValue(String key){
        final Jedis resource = JedisPoolFactory.getInstance().getResource();
        try {
            return resource.get(key);

        } finally {
            resource.close();
        }
    }
    public static void setValue(String key,String value) {
        final Jedis resource = JedisPoolFactory.getInstance().getResource();
        try {
            resource.set(key, value);
            resource.expire(key, 1800);
        } finally {
            resource.close();
        }
    }
    public static void deleteValue(String key) {
        final Jedis resource = JedisPoolFactory.getInstance().getResource();
        try {
            resource.del(key);
        } finally {
            resource.close();
        }
    }

    public static void deleteVal(String key) {
    }
}
