package even.controller;

import even.entity.Student;
import even.mapper.StudentMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class StudentController {
    @Resource
    private StudentMapper studentMapper;

    @GetMapping("/insert")
    public String insert() {
        Student even = new Student(111, "even", 92);
        int res = studentMapper.add(even);
        if (res == 1) {
            return "success";
        } else {
            return "failed";
        }
    }

    @GetMapping("/select")
        public void select(){
         Student result = studentMapper.select(110);
         System.out.println(result);
    }

    @GetMapping("/delete")
        public void delete(){
        studentMapper.delete(111);
    }

    @GetMapping("/update")
        public void update(){
        studentMapper.updateAge(130, 40);
    }


}
