import even.AppMain;
import even.controller.JedisPoolFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppMain.class)
public class testQ2<StudentService> {

    @Test
    public void testSetAndDelete() {
        JedisPoolFactory.setValue("key", "sno1");
        JedisPoolFactory.deleteVal("key");
    }

}
